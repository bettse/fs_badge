#!/usr/bin/env ruby

require 'optparse'
require 'rmagick'
require 'httparty'
require 'awesome_print'

class FS14Badge
  #These are 2 less than the real screen so there is a single pixel border around all sides
  WIDTH  = 262
  HEIGHT = 174

  def initialize(agent)
    @agent = agent
  end

  def send(content)
    image = if File.exists?(content)
      of_image(content)
    else
      of_text(content)
    end
    post(image)
  end

  def of_text(msg)
    Magick::Image.read("caption:#{msg}") do
      self.colorspace = Magick::GRAYColorspace
      self.image_type = Magick::BilevelType
      self.background_color = 'white'
      self.fill = 'black'
      self.stroke = 'black'
      self.size = "#{WIDTH}x#{HEIGHT}"
      self.font = "DejaVu-Serif-Bold"
      self.antialias = false
      self.gravity = Magick::CenterGravity
    end.first
  end

  def of_image(filename)
    Magick::Image.read(filename) do
      self.antialias = false
      self.background_color = 'white'
      self.gravity = Magick::CenterGravity
    end.first.resize_to_fit(WIDTH, HEIGHT).extent(WIDTH, HEIGHT)
  end

  def post(image)
    agent_url = "https://agent.electricimp.com/#{@agent}/image"
    #+2's to compensate for padding
    pixels = image.rotate(180).negate.export_pixels(0, 0, 1+WIDTH+1, 1+HEIGHT+1, 'I')

    options = {
      body: interlace(pixels)
    }
    puts HTTParty.post(agent_url, options)
  end

  def interlace(pixels)
    [pixels.collect{|p| [1, p].min}.join].pack("B*")
  end
end

