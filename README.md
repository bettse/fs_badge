# fs_badge
[![Gitter](https://badges.gitter.im/Join Chat.svg)](https://gitter.im/bettse/fs_badge?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

## Local Control
 * Send text or images to the badge from the command line.  ex `ruby local_control.rb AGENT_ID $(date +"%I:%M %p")`
