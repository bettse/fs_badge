#!/usr/bin/env ruby

require './fs14_badge'
if (ARGV.count < 2)
  puts "Usage: ./local_control.rb AgentId text_or_image_path"
  exit
end

fs = FS14Badge.new(ARGV[0])
fs.send(ARGV[1])
