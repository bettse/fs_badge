
/*****************************************\
*        FUTURESTACK 14 BADGE DEMO        *
*           (c) 2014 New Relic            *
*                                         *
* For more information, see:              *
* github.com/newrelic/futurestack14_badge *
\*****************************************/

server.log("Running at " + http.agenturl());

const BACKEND_HOST = "http://agile-beach-6128.herokuapp.com";
//const BACKEND_HOST = "http://ataraxia.ericbetts.org:4567";

const WIDTH  = 264;
const HEIGHT = 176;

/* HTTP HELPERS -------------------------------------------------------------*/
function http_get(url) {
    local headers = {
        "Accept"       : "application/json",
    }
    
    local req = http.get(BACKEND_HOST + url, headers);
    local res = req.sendsync();
    
    if (res.statuscode != 200) {
        server.log("error getting " + BACKEND_HOST + url);
        return null;
    }
    
    return res.body;
}

function http_post(url, data) {
    if (url[0] != "/") {
      url = "/" + url;
    }
    local body = http.jsonencode(data);
    local headers = {
        "Content-Type" : "application/json",
        "Accept"       : "application/json",
    }

    local req = http.post(BACKEND_HOST + url, headers, body);
    local res = req.sendsync();
    
    if (res.statuscode != 200) {
        server.log("error sending message: " + res.body);
    } else {
        return res.body
    }
}

/* DEVICE EVENT HANDLERS ----------------------------------------------------*/
device.on("screen", function(data) {
    local res = http_post("/text", data);
    
    local data = blob();
    data.writestring(res);
    data.seek(0, 'b');
    
    device.send("screen", data);
});

device.on("remote", function(data) {
  local action = delete data["action"];
  server.log("remote call " + action + " " + http.jsonencode(data));
  local res = http_post(action, data);
  local data = blob();
  data.writestring(res);
  data.seek(0, 'b');
  device.send("screen", data);
  
});

device.on("image", function(data) {
    local res = http_post("/image", data);
    
    local data = blob();
    data.writestring(res);
    data.seek(0, 'b');
    
    device.send("screen", data);
});

device.on("epd", function(data) {
    local res = http_post("/epd", data);
});

/* HTTP HANDLER -------------------------------------------------------------*/
http.onrequest(function(req, res) {
    server.log("Agent got new HTTP Request");
    // we need to set headers and respond to empty requests as they are usually preflight checks
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
    
    if (req.path == "/image" && req.method != "GET") {
        local data = blob(req.len());
        data.writestring(req.body);
        data.seek(0, 'b');
        local len = data.len();
        server.log("Got new image, length " + len);
        res.send(200, "Got new image, length " + len);
        device.send("screen", data.readblob(len));
    } else {
        server.log("Agent got unknown request");
        res.send(200, "OK");
    }
});

